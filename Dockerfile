FROM hasura/graphql-engine:v2.7.0
WORKDIR /hasura
RUN apt update -y && \
    apt install curl -y && \
    curl -L https://github.com/hasura/graphql-engine/raw/stable/cli/get.sh | bash

RUN rm -rf hasura && hasura init hasura